import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  options: any;
  dataProduction1: Array<any>;
  totalHectares: number;
  totalTalhoes: number;
  totalPlantasHa: number;
  totalVargens: number = 50;
  totalSemente: number = 2;
  pesoSementes: number = 0.15;
  totalProdutividade: number;
  constructor(
  ) {
    
    
    // a cada hectare cabe 10 talhões
    // a cada hectare cabe 300 mil plantas
    // a cada talhão cabem 30 mil plantas
    // a cada planta cabem 50 vagens, cada vagem tem 2 semente, a cada 100 sementes o peso médio é de 0.15kg
    //300.000 x 100 (50 x 2) x 0.15 = 4.500 quilos por hectare, ou a cada 10 talhoes o agricultor colhe a 4.500 quilos por safra.
  }

  ngOnInit(): void {
    this.dataProduction1 = [
      { mes: 'jan', data: '10', data2:'20' },
      { mes: 'fev', data: '20', data2:'120' },
      { mes: 'mar', data: '30', data2:'130' },
      { mes: 'abr', data: '40', data2:'140' },
      { mes: 'mai', data: '50', data2:'150' },
      { mes: 'jun', data: '60', data2:'160' },
      { mes: 'jul', data: '70', data2:'170' },
      { mes: 'ago', data: '80', data2:'180' },
      { mes: 'set', data: '90', data2:'190' },
      { mes: 'out', data: '100', data2:'200' },
      { mes: 'nov', data: '110', data2:'210' },
      { mes: 'dez', data: '120', data2:'220' },
    ]

    this.options = {
      legend: {
        data: ['2019', '2020'],
        align: 'left',
      },
      tooltip: {},
      xAxis: {
        data: this.dataProduction1.map((item)=>{return item.mes}),
        silent: false,
        splitLine: {
          show: false,
        },
      },
      yAxis: {},
      series: [
        {
          name: '2019',
          type: 'bar',
          data: this.dataProduction1.map((item)=>{return item.data}),
          animationDelay: (idx) => idx * 10,
        },
        {
          name: '2020',
          type: 'bar',
          data: this.dataProduction1.map((item)=>{ return item.data2}),
          animationDelay: (idx) => idx * 10 + 100,
        },
      ],
      animationEasing: 'elasticOut',
      animationDelayUpdate: (idx) => idx * 5,
    };
  }


  formatLabel(value: number) {
    if (value >= 1000) {
      return Math.round(value / 1000) + 'k';
    }
    this.totalHectares = value;
    
    return (
      this.totalHectares
    )
  }

  onInputChange(event) {
    this.totalTalhoes = event.value * 10;
    this.totalPlantasHa = event.value * 300;
    this.totalProdutividade = this.totalPlantasHa *  this.totalVargens * this.totalSemente * this.pesoSementes;
    console.log(this.totalProdutividade);

  }

}
