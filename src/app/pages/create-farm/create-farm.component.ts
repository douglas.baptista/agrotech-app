import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-create-farm',
  templateUrl: './create-farm.component.html',
  styleUrls: ['./create-farm.component.scss']
})
export class CreateFarmComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  isEditable = true;
  tp_cultivoList: string[] = ['Café', 'Algodão', 'Amendoim', 'Arroz', 'Cacau', 'Cana de açúcar', 'Milho', 'Feijão', 'Mandioca', 'Manga', 'Soja', ];
  tp_soloList: string[] = ['Latossolos', 'Argissolos', 'Neossolos', 'Planossolos', 'Gleissolos', 'Vertissolos', 'Chernossolos']

  constructor(
    private _formBuilder: FormBuilder,
    public _routes: Router
    ) { }

  ngOnInit(): void {

    this.firstFormGroup = this._formBuilder.group({
      tx_nomePropriedade: [''],
      tx_endereco: [''],
      nr_areaTotalHa: [''],
      tp_cultivo: ['']
    });

    this.secondFormGroup = this._formBuilder.group({
      tp_culturaPlantio: [''],
      tp_cultivo:[''],
      tp_solo: ['']
    });

  }

  goToDashboardPage() {
    this._routes.navigate(['/dashboard']);
  }



}


