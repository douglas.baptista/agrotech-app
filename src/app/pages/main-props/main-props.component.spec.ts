import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainPropsComponent } from './main-props.component';

describe('MainPropsComponent', () => {
  let component: MainPropsComponent;
  let fixture: ComponentFixture<MainPropsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainPropsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPropsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
