import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-props',
  templateUrl: './main-props.component.html',
  styleUrls: ['./main-props.component.scss']
})
export class MainPropsPageComponent implements OnInit {

  constructor(
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  goToMainPropsPage() {
    this.router.navigate(['/mainProps']);
  }

  goToCreateFarmPage() {
    this.router.navigate(['/create-farm']);
  }

}

