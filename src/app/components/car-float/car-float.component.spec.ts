import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarFloatComponent } from './car-float.component';

describe('CarFloatComponent', () => {
  let component: CarFloatComponent;
  let fixture: ComponentFixture<CarFloatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarFloatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarFloatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
