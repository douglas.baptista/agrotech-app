import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateFarmComponent } from './pages/create-farm/create-farm.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';

//Pages
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { MainPropsPageComponent } from './pages/main-props/main-props.component';

const routes: Routes = [
  { path: '',  redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginPageComponent },
  { path: 'mainProps', component: MainPropsPageComponent},
  {path: 'create-farm', component: CreateFarmComponent},
  {path:'dashboard', component: DashboardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

